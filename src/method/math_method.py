
from __future__ import print_function
import csv
import xlsxwriter

# 'sentiment_score', row[0]
#  'sentiment_label', row[1]
# 'emotion_sadness', row[2]
# 'emotion_joy', row[3]
# 'emotion_fear', row[4]
#  'emotion_disgust', row[5]
#  'emotion_anger', row[6]
#  'influenza', row[7]
#  'sick', row[8]
#  'flu', row[9]
#  'headache', row[10]
#   'sore_throat', row[11]
#  'coughing', row[12]
#  'fever',  row[13]
# 'runny_nose'] row[14]

fileLikelihood = "C:/Users/Rialda/Documents/pyTrack-master/pyTrack-master/src/method/likelihood.csv"
fileIliLabel = "C:/Users/Rialda/Documents/pyTrack-master/pyTrack-master/src/method/ili_label.csv"


def math_method():
    f_ili_label = open(fileIliLabel, 'a')
    f_likelihood = open(fileLikelihood, 'a')
    workbook = xlsxwriter.Workbook('results.xlsx')
    treshold = 0.95
    results_label_file = 'C:/Users/Rialda/Documents/pyTrack-master/pyTrack-master/src/method/'+str(treshold)+'results_label.csv'
    results_likelihood_file = 'C:/Users/Rialda/Documents/pyTrack-master/pyTrack-master/src/method/'+str(treshold)+'results_likelihood.csv'
    results_ili_label_file = open(results_label_file, "a")
    results_ili_likelihood_file = open(results_likelihood_file, "a")

    with open('C:/Users/Rialda/Documents/pyTrack-master/pyTrack-master/src/method/formula_features.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)

        for row in csvReader:
            sentiment_score = row[0]
            sentiment_label = row[1]
            emotion_sadness = row[2]
            emotion_joy = row[3]
            emotion_fear = row[4]
            emotion_disgust = row[5]
            emotion_anger = row[6]

            flu = row[8]
            influenza = row[9]

            sore_throat = row[10]
            runny_nose = row[11]
            fatigue = row[12]
            sneezing = row[13]
            coughing = row[14]
            fever = row[15]
            headache = row[16]

            sore_throat_value = 0
            runny_nose_value = 0
            fatigue_value = 0
            sneezing_value = 0
            coughing_value = 0
            fever_value = 0
            headache_value = 0

            k_max = 10
            sy_max = 12
            se_max = 3
            e_max = 5

            # KEY_SUM FOR THE KEYWORD WEIGHT
            if influenza == '1' or flu == '1':
                key_sum = 10
            else:
                key_sum = 0

            # WEIGHTS OF SYMPTOMS
            if fever == '1':
                fever_value = 5
            else:
                fever_value = 0

            if coughing == '1':
                coughing_value = 3
            else:
                coughing_value = 0

            if sore_throat == '1':
                sore_throat_value = 2
            else:
                sore_throat_value = 0

            if runny_nose == '1':
                runny_nose_value = 0.5
            else:
                runny_nose_value = 0

            if headache == '1':
                headache_value = 0.5
            else:
                headache_value = 0

            if fatigue == '1':
                fatigue_value = 0.5
            else:
                fatigue_value = 0.5

            if sneezing == '1':
                sneezing_value = 0.5
            else:
                sneezing_value = 0.5

            # SYMPTOM_SCORE FOR THE SUM OF SYMPTOM WEIGHTS
            symptoms_score = fever_value + coughing_value + sore_throat_value + runny_nose_value + sneezing_value + headache_value + fatigue_value

            # SENTIMENT VALUES
            if float(sentiment_score) == 0:
                sentiment_value = 2
            if float(sentiment_score) > 0:
                sentiment_value = 1
            else:
                sentiment_value = 3

            emotions = [emotion_anger, emotion_disgust, emotion_fear, emotion_joy, emotion_sadness]
            max_emo = max(emotions)
            if max_emo == emotion_sadness:
                emotional_value = 5
            elif max_emo == emotion_fear or max_emo == emotion_anger:
                emotional_value = 4
            elif max_emo == emotion_disgust:
                emotional_value = 2
            elif max_emo == emotion_joy:
                emotional_value = 1
            else:
                emotional_value = 0

            total = k_max + sy_max + se_max + e_max
            ili_likelihood = (key_sum + symptoms_score + sentiment_value + emotional_value) / total

            if ili_likelihood <= treshold:
                ili_label = 'false'
            else:
                ili_label = 'true'

            results_ili_label_file.write(str(ili_label)+'\n')
            results_ili_likelihood_file.write(str(ili_likelihood) + '\n')
        print("Writing into files complete")





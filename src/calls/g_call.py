from flask import jsonify
from scipy import stats

cdc_normalized = [0.031044601,
    0.039730047,
    0.048826291,
    0.061678404,
    0.079460094,
    0.095833333,
    0.194131455,
    0.374002347,
    0.541549296,
    0.605633803,
    0.756690141,
    0.787617371,
    0.905340376,
    0.976584507,
    1,
    0.805751174,
    0.539377934,
    0.363438967,
    0.247828638,
    0.253873239,
    0.209800469,
    0.197007042,
    0.146126761,
    0.120657277,
    0.082335681,
    0.055633803
    ]


def google_api():
    results_ili_spear = open("C:/Users/Rialda/Documents/pyTrack-master/pyTrack-master/src/calls/results_watson_spear.csv", "a")

    watson_norm = [
        0,
        0.5,
        0.416666667,
        0.416666667,
        0.25,
        0.583333333,
        0.5,
        0.583333333,
        0.333333333,
        0.416666667,
        0.5,
        1,
        0.416666667,
        0.75,
        0.583333333,
        0.75,
        0.416666667,
        0.416666667,
        0.166666667,
        0.25,
        0.083333333,
        0.333333333,
        0.25,
        0.5,
        0.166666667,
        0.583333333,
    ]

    spear_correl = stats.spearmanr(cdc_normalized, watson_norm)
    writing = ("0.95 Watson ", spear_correl)
    results_ili_spear.write(str(writing) + '\n')
    return writing

